﻿@neg1_min = 0.4
@neg1_max = 0.5

@neg2_min = 0.3
@neg2_max = 0.4

@neg3_min = 0.1
@neg3_max = 0.3

@pos1_min = 0.5
@pos1_max = 0.6

@pos2_min = 0.6
@pos2_max = 0.7

@pos3_min = 0.7
@pos3_max = 0.9

@beauty1min = 0.35
@beauty1max = 0.65

@beauty2min = 0.4
@beauty2max = 0.6

@beauty3min = 0.45
@beauty3max = 0.55

@blend1min = 0.0
@blend1max = 0.2

@blend2min = 0.2
@blend2max = 0.5

@blend3min = 0.5
@blend3max = 0.8

q_arab = {

	template = "arab"
	skin_color = {
		10 = { 0.95 0.5 1.0 0.65 }
		2 = { 0.3 0.5 0.7 0.65 }
	}
	
	hair_color = {
		# Brown
		5 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		95 = { 0.01 0.9 0.5 0.99 }
		# white
		30 = {0.0 0.0 0.02 0.25 } 
	}

	
	gene_bs_ear_elf = {
		10 = { name = ear_elf  range = {0.3 1.0} }
	
	}

} 



q_african = {

	template = "african"
	skin_color = {
		10 = { 0.95 0.7 1.0 0.85 }
		2 = { 0.4 0.7 0.7 0.85 }
	}
	
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
		# Brown
		2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
		# white
		80 = {0.0 0.0 0.02 0.25 } 
	}

	
	gene_bs_ear_elf = {
		10 = { name = ear_elf  range = {0.3 1.0} }
	
	}

} 

q_asian = {

	template = "asian"
	skin_color = {
		10 = { 0.95 0.3 1.0 0.5 }
		2 = { 0.5 0.3 1.0 0.5 }
	}
	
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
	# Brown
		2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
		# white
		80 = {0.0 0.0 0.02 0.25 } 
	}

	
	gene_bs_ear_elf = {
		10 = { name = ear_elf  range = {0.3 1.0} }
	
	}
}

qbyzantine = {

	template = "byzantine"
	skin_color = {
		10 = { 0.95 0.25 1.0 0.4 }
		2 =  { 0.0 0.25 0.5 0.4 } 
	}
	
	hair_color = {
		# Blonde
		20 = { 0.4 0.25 0.75 0.5 }
		# Brown
		20 = { 0.65 0.5 0.9 0.8 }
		# Red
		10 = { 0.85 0.0 1.0 0.5 }
		# Black
		20 = { 0.0 0.9 0.5 1.0 }
		# white
		80 = {0.0 0.0 0.02 0.25 } 
	}

	
	gene_bs_ear_elf = {
		10 = { name = ear_elf  range = {0.3 1.0} }
	
	}

}  

q_causasian_blond = {

	template = "caucasian_blond"
	skin_color = {
		10 = { 0.95 0.25 1.0 0.4 }
		2 =  { 0.0 0.25 0.5 0.4 } 
	}
	
	hair_color = {
		# Blonde
		10 = { 0.25 0.2 0.6 0.55 }
		# # Brown
		# 40 = { 0.65 0.45 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# # Black
		# 30 = { 0.0 0.9 0.5 1.0 }
		# white
		80 = {0.0 0.0 0.02 0.25 } 
	}

	
	gene_bs_ear_elf = {
		10 = { name = ear_elf  range = {0.3 1.0} }
	
	}

}  

q_causasian_ginger = {

	template = "caucasian_ginger"
	skin_color = {
		10 = { 0.95 0.25 1.0 0.4 }
		2 =  { 0.0 0.25 0.5 0.4 } 
	}
	
	hair_color = {
			# # Blonde
		# 10 = { 0.25 0.2 0.75 0.25 }
		# # Brown
		# 40 = { 0.65 0.45 0.9 1.0 }
		# Red
		20 = { 0.7 0.3 0.95 0.5 }
		# Auburn
		10 = { 0.8 0.55 0.95 0.8 }
		# # Black
		# 30 = { 0.0 0.9 0.5 1.0 }
		# white
		80 = {0.0 0.0 0.02 0.25 } 
	}

	
	gene_bs_ear_elf = {
		10 = { name = ear_elf  range = {0.3 1.0} }
	
	}

}  

q_causasian_brown_hair = {

	template = "caucasian_brown_hair"
	skin_color = {
		10 = { 0.95 0.25 1.0 0.4 }
		2 =  { 0.0 0.25 0.5 0.4 } 
	}
	
	hair_color = {
		# # Blonde
		# 10 = { 0.25 0.2 0.75 0.25 }
		# Brown
		40 = { 0.65 0.65 0.9 0.8 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# # Black
		# 30 = { 0.0 0.9 0.5 1.0 }
		# white
		80 = {0.0 0.0 0.02 0.25 } 
	}

	
	gene_bs_ear_elf = {
		10 = { name = ear_elf  range = {0.3 1.0} }
	
	}

}  

q_causasian_dark_hair = {

	template = "caucasian_dark_hair"
	skin_color = {
		10 = { 0.95 0.25 1.0 0.4 }
		2 =  { 0.0 0.25 0.5 0.4 } 
	}
	
	hair_color = {
		# # Blonde
		# 10 = { 0.25 0.2 0.75 0.25 }
		# # Brown
		# 40 = { 0.65 0.45 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		30 = { 0.01 0.9 0.5 0.99 }
		# white
		80 = {0.0 0.0 0.02 0.25 } 
	}

	
	gene_bs_ear_elf = {
		10 = { name = ear_elf  range = {0.3 1.0} }
	
	}

}  

q_indian = {

	template = "indian"
	skin_color = {
		10 = { 0.95 0.5 1.0 0.7 }
		2 =  { 0.3 0.5 0.5 0.7 }
	}
	
	hair_color = {
		# Brown
		2 = { 0.65 0.8 0.9 1.0 }
		# # Red
		 2 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
		# white
		80 = {0.0 0.0 0.02 0.25 } 
	}

	
	gene_bs_ear_elf = {
		10 = { name = ear_elf  range = {0.3 1.0} }
	
	}

}  

q_mediterranean = {

	template = "mediterranean"
	skin_color = {
		10 = { 0.95 0.4 1.0 0.5 }
		2 =  { 0.5 0.4 0.8 0.5 }
	}
	
	hair_color = {
		# Blonde
		2 = { 0.25 0.4 0.75 0.65 }
		# Brown
		33 = { 0.65 0.7 0.9 1.0 }
		# # Red
		2 = { 0.85 0.0 1.0 0.5 }
		# Black
		65 = { 0.01 0.9 0.5 0.99 }
		# white
		80 = {0.0 0.0 0.02 0.25 } 
	}

	
	gene_bs_ear_elf = {
		10 = { name = ear_elf  range = {0.3 1.0} }
	
	}

} 