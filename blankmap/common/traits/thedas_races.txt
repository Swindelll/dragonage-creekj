
tdwarf = {
	index = 700
	prowess = 1
	opposites = {
		qunari
		human
		elf
	}	

	inherit_chance = 25
	both_parent_has_trait_inherit_chance = 100
	genetic = yes
	physical = yes

	fertility = -0.3
	martial = 1
	stewardship = 1
	birth = 0
	random_creation = 0.5
	health = 0.5
	negate_health_penalty_add = 0.10
	
icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = tdwarf.dds
			}
			desc = tdwarf.dds
		}
	}	
	same_opinion = 20

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_tdwarf_desc
			}
			desc = trait_tdwarf_character_desc
		}
	}
}

qunari = {

index = 701
	opposites = {
		tdwarf
		human
		elf
	}

	martial = 1
	prowess = 2
	monthly_prestige = 0.25
	dread_baseline_add = 2
	negate_health_penalty_add = 0.25


	inherit_chance = 25
	both_parent_has_trait_inherit_chance = 100
	genetic = yes
	physical = yes


	birth = 0
	random_creation = 0.5
	
	
	
	
	vassal_opinion = 5
	same_opinion = 20

icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = qunari.dds
			}
			desc = qunari.dds
		}
	}	

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_qunari_desc
			}
			desc = trait_qunari_character_desc
		}
	}
}

human = {
index = 702
	opposites = {
		tdwarf
		qunari
		elf
	}

	diplomacy = 1
	prowess = 1
	monthly_prestige = 0.25
	dread_baseline_add = 2
	negate_health_penalty_add = 0.25


	inherit_chance = 25
	both_parent_has_trait_inherit_chance = 100
	genetic = yes
	physical = yes


	birth = 0
	random_creation = 0.5
	

	
	attraction_opinion = -5
	
	vassal_opinion = 5
	same_opinion = 20

icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = human.dds
			}
			desc = human.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_human_desc
			}
			desc = trait_human_character_desc
		}
	}
}

elf = {
index = 703
	opposites = {
		tdwarf
		human
		qunari
	}
	Learning = 1
	intrigue = 1

	
	negate_health_penalty_add = 0.25


	inherit_chance = 0
	both_parent_has_trait_inherit_chance = 100
	#genetic = yes
	physical = yes


	birth = 0
	random_creation = 0.5
	
	
	
	attraction_opinion = 15
	
	vassal_opinion = 5
	same_opinion = 20
icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = elf.dds
			}
			desc = elf.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_elf_desc
			}
			desc = trait_elf_character_desc
		}
	}
}

