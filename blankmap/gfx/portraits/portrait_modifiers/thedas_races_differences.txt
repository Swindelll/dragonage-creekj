race_diff= {
	usage = game
#gene_bs_ear_elf
	tdwarf = {
		ignore_outfit_tags = yes
		dna_modifiers = {
			morph = {
				mode = modify_multiply
				gene = gene_height
				value = 0.5
			}
			morph = {
				mode = modify_multiply
				gene = gene_bs_ear_elf
				value = 0
			}
		}
		weight = {
			base = 0
			modifier = {
				add = 100
				has_trait = tdwarf
			}
		}
	}
	
	qunari = {
		ignore_outfit_tags = yes
		dna_modifiers = {
			morph = {
				mode = modify_multiply
				gene = gene_height
				value = 1.50
			}
		}
		weight = {
			base = 0
			modifier = {
				add = 100
				has_trait = qunari
			}
		}
	}
	
	human = {
		ignore_outfit_tags = yes
		dna_modifiers = {
			morph = {
				mode = modify_multiply
				gene = gene_bs_ear_elf
				value = 0
			}
		}
		weight = {
			base = 0
			modifier = {
				add = 100
				has_trait = human
			}
		}
	}
	
	elf = {
		ignore_outfit_tags = yes
		dna_modifiers = {
			morph = {
				mode = modify_multiply
				gene = gene_height
				value = 0.9
			}
		}
		weight = {
			base = 0
			modifier = {
				add = 100
				has_trait = elf
			}
		}
	}
	
}